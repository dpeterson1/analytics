.snowflake_dbt_jobs: &snowflake_dbt_jobs
  image: registry.gitlab.com/gitlab-data/dbt-image:v0.0.3
  stage: ⚙️ dbt Run
  before_script:
    - cd transform/snowflake-dbt/
    - echo $BRANCH_NAME
    - if [ $BRANCH_NAME = "master" ]; then export SNOWFLAKE_PREP_DATABASE="PREP"; else export SNOWFLAKE_PREP_DATABASE="${CI_COMMIT_REF_NAME^^}_PREP"; fi
    - if [ $BRANCH_NAME = "master" ]; then export SNOWFLAKE_PROD_DATABASE="PROD"; else export SNOWFLAKE_PROD_DATABASE="${CI_COMMIT_REF_NAME^^}_PROD"; fi
    - echo $SNOWFLAKE_PREP_DATABASE
    - echo $SNOWFLAKE_PROD_DATABASE
    - export SNOWFLAKE_LOAD_DATABASE="RAW" # dbt pulls from RAW
    - echo $SNOWFLAKE_LOAD_DATABASE
    - export SNOWFLAKE_TRANSFORM_WAREHOUSE=$SNOWFLAKE_MR_XS_WAREHOUSE
    - echo $SNOWFLAKE_TRANSFORM_WAREHOUSE
    - export SNOWFLAKE_SNAPSHOT_DATABASE="SNOWFLAKE"
    - echo $SNOWFLAKE_SNAPSHOT_DATABASE
    - export ENVIRONMENT="CI"
    - echo $ENVIRONMENT
    - export CI_PROFILE_TARGET="--profiles-dir profile --target ci"
    - echo $CI_PROFILE_TARGET
    - export DBT_RUNNER="${GITLAB_USER_ID}-${CI_MERGE_REQUEST_ID}-${CI_JOB_ID}"
    - echo $DBT_RUNNER
    - mkdir -p $CI_PROJECT_DIR/transform/snowflake-dbt/reference_state/
    - curl -o $CI_PROJECT_DIR/transform/snowflake-dbt/reference_state/manifest.json https://dbt.gitlabdata.com/manifest.json
    - mkdir -p ~/.ssh
    - touch ~/.ssh/id_rsa
    - chmod 700 ~/.ssh
    - echo "$GIT_DATA_TESTS_SSH_PRIVATE_KEY" | base64 --decode > ~/.ssh/id_rsa # decodes key from base64
    - chmod 0400 ~/.ssh/id_rsa # Makes key read only
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config # Adds gitlab.com as known host
  after_script:
    - cd $CI_PROJECT_DIR/transform/snowflake-dbt/
    - mkdir -p $CI_PROJECT_DIR/public/dbt/
    - cp -r target $CI_PROJECT_DIR/public/dbt/
  tags:
    - analytics
  only:
    - merge_requests
  when: manual
  artifacts:
    name: "dbt Compiled Files and logs"  
    paths:
      - public
      - transform/snowflake-dbt/logs
    expire_in: 1 week
    when: always

# Common commands anchors
.clone_raw: &clone_raw
  - export SNOWFLAKE_LOAD_DATABASE="${CI_COMMIT_REF_NAME^^}_RAW"
  - echo $SNOWFLAKE_LOAD_DATABASE

.set_snapshot: &set_snapshot
  - export SNOWFLAKE_SNAPSHOT_DATABASE="${CI_COMMIT_REF_NAME^^}_RAW" # Must write to clone of RAW since this does real work
  - echo $SNOWFLAKE_SNAPSHOT_DATABASE

.dbt_deps: &dbt_deps
  - dbt deps $CI_PROFILE_TARGET

.dbt_validate_tags: &dbt_validate_tags
  - dbt --warn-error run-operation tag_validation --profiles-dir profile --target ci

# First checks if dbt has already been seeded using the first table defined in ./data/seeds.yml before seeding.
.auto_dbt_seed: &auto_dbt_seed
    - SCHEMA_NAME=$(python3 ../../orchestration/yaml_reader.py read_seed_schema --dbt_project_path "./dbt_project.yml")
    - TABLE_NAME=$(python3 ../../orchestration/yaml_reader.py read_seed_name --seed_file_path "./data/seeds.yml")
    - RESULT=$(python3 ../../orchestration/manage_snowflake.py check_if_table_exists --database $SNOWFLAKE_PROD_DATABASE --schema $SCHEMA_NAME --table_name $TABLE_NAME)
    - CLEAN_RESULT=$(echo $RESULT | egrep -o '.{4,5}$') #extract only the result from the python output.
    - if [ $CLEAN_RESULT = "False" ]; then dbt seed --full-refresh $CI_PROFILE_TARGET; fi

.deps_and_seed: &deps_and_seed
  - *dbt_deps
  - *auto_dbt_seed
  - *dbt_validate_tags

.l_warehouse: &l_warehouse
  - export SNOWFLAKE_TRANSFORM_WAREHOUSE=$SNOWFLAKE_MR_L_WAREHOUSE
  - echo $SNOWFLAKE_TRANSFORM_WAREHOUSE

.xl_warehouse: &xl_warehouse
  - export SNOWFLAKE_TRANSFORM_WAREHOUSE=$SNOWFLAKE_MR_XL_WAREHOUSE
  - echo $SNOWFLAKE_TRANSFORM_WAREHOUSE

.xs_warehouse: &xs_warehouse
  - export SNOWFLAKE_TRANSFORM_WAREHOUSE=$SNOWFLAKE_MR_XS_WAREHOUSE
  - echo $SNOWFLAKE_TRANSFORM_WAREHOUSE


🏗️🏭build_changes:
  <<: *snowflake_dbt_jobs
  script:
    - set +e
    - export SNOWFLAKE_TRANSFORM_WAREHOUSE=${WAREHOUSE:-$SNOWFLAKE_MR_XL_WAREHOUSE}
    - echo $SNOWFLAKE_TRANSFORM_WAREHOUSE
    - export SNOWFLAKE_SNAPSHOT_DATABASE="${CI_COMMIT_REF_NAME^^}_RAW" # Build can execute snapshots
    - echo $SNOWFLAKE_SNAPSHOT_DATABASE
    - if [ ! -z "$RAW_DB" ]; then SNOWFLAKE_LOAD_DATABASE="${CI_COMMIT_REF_NAME^^}_RAW"; fi
    - echo $SNOWFLAKE_LOAD_DATABASE
    - export DBT_CI_STATE="--state reference_state"
    - echo $DBT_CI_STATE
    - DBT_CI_CHANGES=$(git diff --name-only $CI_MERGE_REQUEST_DIFF_BASE_SHA...HEAD | xargs -n1 basename | grep '\.sql\|\.csv'  | sed -E "s/(.sql|.csv)/$DOWNSTREAM/g" )
    # Getting the most current reference state from the main branch will include any changes from other merged branches
    # If we can get the reference from the HEAD of the branch this could be solved.
    # - export DBT_CI_SELECTION="state:modified."${STATE_MODIFIED:-"body"}${DOWNSTREAM:-""}
    - export DBT_CI_SELECTION=""
    - if [ -z "$SELECTION" ]; then DBT_CI_SELECTION=$DBT_CI_CHANGES; else DBT_CI_SELECTION=$SELECTION; fi
    - echo $DBT_CI_SELECTION
    - if [ -z "$DBT_CI_SELECTION" ]; then echo "No models selected." && exit 3; fi 
    - export DBT_CI_FAIL_FAST=""
    - if [ -z "$FAIL_FAST" ]; then DBT_CI_FAIL_FAST="--fail-fast"; fi
    - echo $DBT_CI_FAIL_FAST
    - export DBT_CI_EXCLUDE=""
    - if [ ! -z "$EXCLUDE" ]; then DBT_CI_EXCLUDE="--exclude $EXCLUDE"; fi
    - echo $DBT_CI_EXCLUDE
    - export DBT_CI_FULL_REFRESH=""
    - if [ ! -z "$FULL_REFRESH" ]; then DBT_CI_FULL_REFRESH="--full-refresh"; fi
    - echo $DBT_CI_FULL_REFRESH
    # VARS format "key":"value","key":"value"
    - export DBT_CI_VARS=""
    - if [ ! -z "$VARS" ]; then DBT_CI_VARS="--vars {$VARS}"; fi
    - echo $DBT_CI_VARS
    - dbt deps $CI_PROFILE_TARGET
    - dbt $DBT_CI_FAIL_FAST clone --select $DBT_CI_SELECTION $DBT_CI_EXCLUDE $CI_PROFILE_TARGET $DBT_CI_FULL_REFRESH $DBT_CI_STATE || FAILED=true
    - if [ $FAILED ]; then echo "Cloning process failed" && exit 4; fi
    - dbt $DBT_CI_FAIL_FAST build --select $DBT_CI_SELECTION $DBT_CI_EXCLUDE $CI_PROFILE_TARGET $DBT_CI_FULL_REFRESH --defer $DBT_CI_STATE  $DBT_CI_VARS || FAILED=true
    - if [ $FAILED ]; then echo "Build process failed" && exit 5; fi

🎛️custom_invocation:
  <<: *snowflake_dbt_jobs
  script:
    - set +e
    - export SNOWFLAKE_SNAPSHOT_DATABASE="${CI_COMMIT_REF_NAME^^}_RAW" # Snapshot should never run in RAW
    - if [ -z $WAREHOUSE ]; then echo "No Warehouse selected." && exit 7; else SNOWFLAKE_TRANSFORM_WAREHOUSE=$WAREHOUSE; fi
    - echo $SNOWFLAKE_TRANSFORM_WAREHOUSE
    - export DBT_CI_STATEMENT=""
    - if [ -z "$STATEMENT" ]; then echo "No models selected." && exit 3; else DBT_CI_STATEMENT=$STATEMENT; fi
    - echo $DBT_CI_STATEMENT
    - echo $SNOWFLAKE_SNAPSHOT_DATABASE
    - dbt deps $CI_PROFILE_TARGET
    - dbt $DBT_CI_STATEMENT $CI_PROFILE_TARGET || FAILED=true
    - if [ $FAILED ]; then echo "Invocation process failed" && exit 5; fi
  

📚✏️generate_dbt_docs:
  image: registry.gitlab.com/gitlab-data/dbt-image:v0.0.3
  stage: ⚙️ dbt Run
  before_script:
    - cd transform/snowflake-dbt/
    - echo $BRANCH_NAME
    - export SNOWFLAKE_PREP_DATABASE="RAW"
    - export SNOWFLAKE_PROD_DATABASE="RAW"
    - export SNOWFLAKE_LOAD_DATABASE="RAW"
    - echo $SNOWFLAKE_LOAD_DATABASE
    - export SNOWFLAKE_TRANSFORM_WAREHOUSE=$SNOWFLAKE_MR_XS_WAREHOUSE
    - echo $SNOWFLAKE_TRANSFORM_WAREHOUSE
    - export SNOWFLAKE_SNAPSHOT_DATABASE="SNOWFLAKE"
    - echo $SNOWFLAKE_SNAPSHOT_DATABASE
    - export ENVIRONMENT="CI"
    - echo $ENVIRONMENT
    - export DBT_RUNNER="${GITLAB_USER_ID}-${CI_MERGE_REQUEST_ID}"
    - echo $DBT_RUNNER
    - export CI_PROFILE_TARGET="--profiles-dir profile --target ci"
    - echo $CI_PROFILE_TARGET
    - mkdir -p ~/.ssh
    - touch ~/.ssh/id_rsa
    - chmod 700 ~/.ssh
    - echo "$GIT_DATA_TESTS_SSH_PRIVATE_KEY" | base64 --decode > ~/.ssh/id_rsa # decodes key from base64
    - chmod 0400 ~/.ssh/id_rsa # Makes key read only
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config # Adds gitlab.com as known host
  variables:
    LOGURU_COLORIZE: "true"
  when: manual
  allow_failure: true
  only:
    changes:
      - "transform/snowflake-dbt/**/*.{md,yml}"
    refs:
      - merge_request
  script:
    - *dbt_deps
    - dbt docs generate $CI_PROFILE_TARGET --no-compile || FAILED=true
    - if [ $FAILED ]; then exit 1; fi
  after_script:
    - cd $CI_PROJECT_DIR/transform/snowflake-dbt/
    - mkdir -p $CI_PROJECT_DIR/public/dbt/
    - cp -r target $CI_PROJECT_DIR/public/dbt/
  tags:
    - analytics
  
# dbt tests
.dbt_misc_jobs: &dbt_misc_jobs
  <<: *snowflake_dbt_jobs
  stage: 🛠 dbt Misc

🧠all_tests:
  <<: *dbt_misc_jobs
  script:
    - *deps_and_seed
    - dbt test $CI_PROFILE_TARGET

💾data_tests:
  <<: *dbt_misc_jobs
  script:
    - *deps_and_seed
    - dbt test --data $CI_PROFILE_TARGET

🌻freshness:
  <<: *dbt_misc_jobs
  script:
    - *deps_and_seed
    - dbt source snapshot-freshness $CI_PROFILE_TARGET

🗂schema_tests:
  <<: *dbt_misc_jobs
  script:
    - *deps_and_seed
    - dbt test --schema $CI_PROFILE_TARGET

📸snapshots:
  <<: *dbt_misc_jobs
  script:
    - export SNOWFLAKE_SNAPSHOT_DATABASE="${CI_COMMIT_REF_NAME^^}_RAW" # Must write to clone of RAW since this does real work
    - echo $SNOWFLAKE_SNAPSHOT_DATABASE
    - *deps_and_seed
    - dbt snapshot $CI_PROFILE_TARGET

📝specify_tests:
  <<: *dbt_misc_jobs
  script:
    - *deps_and_seed
    - echo $DBT_MODELS
    - dbt test $CI_PROFILE_TARGET --models $DBT_MODELS

🌱manual_seed:
  <<: *snowflake_dbt_jobs
  stage: 🛠 dbt Misc
  script:
    - dbt deps $CI_PROFILE_TARGET
    - dbt seed --full-refresh $CI_PROFILE_TARGET #seed data from csv
    - dbt --warn-error run-operation tag_validation --profiles-dir profile --target ci

# ======
# SAFE Models Check
# ======

.safe_models_check: &safe_models_check
  stage: 🛠 dbt Misc
  image: registry.gitlab.com/gitlab-data/dbt-image:v0.0.3
  before_script:
    - cd transform/snowflake-dbt/
    - echo $BRANCH_NAME
    - if [ $BRANCH_NAME = "master" ]; then export SNOWFLAKE_PREP_DATABASE="PREP"; else export SNOWFLAKE_PREP_DATABASE="${CI_COMMIT_REF_NAME^^}_PREP"; fi
    - if [ $BRANCH_NAME = "master" ]; then export SNOWFLAKE_PROD_DATABASE="PROD"; else export SNOWFLAKE_PROD_DATABASE="${CI_COMMIT_REF_NAME^^}_PROD"; fi
    - echo $SNOWFLAKE_PREP_DATABASE
    - echo $SNOWFLAKE_PROD_DATABASE
    - export SNOWFLAKE_LOAD_DATABASE="RAW" # dbt pulls from RAW
    - echo $SNOWFLAKE_LOAD_DATABASE
    - export SNOWFLAKE_TRANSFORM_WAREHOUSE=$SNOWFLAKE_MR_XS_WAREHOUSE
    - echo $SNOWFLAKE_TRANSFORM_WAREHOUSE
    - export SNOWFLAKE_SNAPSHOT_DATABASE="SNOWFLAKE"
    - echo $SNOWFLAKE_SNAPSHOT_DATABASE
    - export CI_PROFILE_TARGET="--profiles-dir profile --target ci"
    - echo $CI_PROFILE_TARGET
    - export DBT_RUNNER="${GITLAB_USER_ID}-${CI_MERGE_REQUEST_ID}"
    - echo $DBT_RUNNER
    - mkdir -p ~/.ssh
    - touch ~/.ssh/id_rsa
    - chmod 700 ~/.ssh
    - echo "$GIT_DATA_TESTS_SSH_PRIVATE_KEY" | base64 --decode > ~/.ssh/id_rsa # decodes key from base64
    - chmod 0400 ~/.ssh/id_rsa # Makes key read only
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config # Adds gitlab.com as known host
  tags:
    - analytics
  only:
    changes:
      - "transform/snowflake-dbt/models/**/*.sql"
    refs:
      - merge_request
  allow_failure: false

🚫safe_model_script:
  <<: *safe_models_check
  script:
    - dbt deps $CI_PROFILE_TARGET
    - dbt --quiet ls $CI_PROFILE_TARGET --models tag:mnpi+
      --exclude
        tag:mnpi_exception
        config.database:$SNOWFLAKE_PREP_DATABASE
        config.schema:restricted_safe_common
        config.schema:restricted_safe_common_mapping
        config.schema:restricted_safe_common_mart_finance
        config.schema:restricted_safe_common_mart_sales
        config.schema:restricted_safe_common_mart_marketing
        config.schema:restricted_safe_common_mart_product
        config.schema:restricted_safe_common_prep
        config.schema:restricted_safe_legacy
        config.schema:restricted_safe_workspace_finance
        config.schema:restricted_safe_workspace_sales 
        config.schema:restricted_safe_workspace_marketing
        config.schema:restricted_safe_workspace_engineering
      --output json > safe_models.json
    - python3 safe_model_check.py

# ======
# Macro name Check
# ======

.dbt_macro_check: &dbt_macro_check
  stage: 🛠 dbt Misc
  image: registry.gitlab.com/gitlab-data/dbt-image:v0.0.3
  before_script:
    - cd transform/snowflake-dbt/
  tags:
    - analytics
  only:
    changes:
      - "transform/snowflake-dbt/macros/**/*.sql"
    refs:
      - merge_request
  allow_failure: false

🔍macro_name_check:
  <<: *dbt_macro_check
  script:
    - python3 macro_name_check.py

# ======
# Tableau Model Check
# ======

.tableau_direct_dependencies_query: &tableau_direct_dependencies_query
  stage: 🛠 dbt Misc
  image: registry.gitlab.com/gitlab-data/data-image/data-image:v2.0.7
  tags:
    - analytics
  only:
    changes:
      - "transform/snowflake-dbt/models/**/*.sql"
    refs:
      - merge_request
  allow_failure: true

🔍tableau_direct_dependencies_query:
  <<: *tableau_direct_dependencies_query
  script:
    - DIFF=$(git diff origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME...HEAD --name-only | grep -iEo "(.*)\.sql" | sed -E 's/\.sql//' | awk -F '/' '{print tolower($NF)}' | sort | uniq )
    - echo $DIFF
    - python3 orchestration/tableau_dependency_query/src/tableau_query.py $DIFF
  variables:
    LOGURU_COLORIZE: "true"

# ======
# Lint dbt SQL
# ======
.dbt_sql_lint: &dbt_sql_lint
  stage: 🛠 dbt Misc
  image: registry.gitlab.com/gitlab-data/dbt-image:v0.0.3
  before_script:
    - cd transform/snowflake-dbt/
    - echo $BRANCH_NAME
    - if [ $BRANCH_NAME = "master" ]; then export SNOWFLAKE_PREP_DATABASE="PREP"; else export SNOWFLAKE_PREP_DATABASE="${CI_COMMIT_REF_NAME^^}_PREP"; fi
    - if [ $BRANCH_NAME = "master" ]; then export SNOWFLAKE_PROD_DATABASE="PROD"; else export SNOWFLAKE_PROD_DATABASE="${CI_COMMIT_REF_NAME^^}_PROD"; fi
    - echo $SNOWFLAKE_PREP_DATABASE
    - echo $SNOWFLAKE_PROD_DATABASE
    - export SNOWFLAKE_LOAD_DATABASE="RAW" # dbt pulls from RAW
    - echo $SNOWFLAKE_LOAD_DATABASE
    - export SNOWFLAKE_TRANSFORM_WAREHOUSE=$SNOWFLAKE_MR_XS_WAREHOUSE
    - echo $SNOWFLAKE_TRANSFORM_WAREHOUSE
    - export SNOWFLAKE_SNAPSHOT_DATABASE="SNOWFLAKE"
    - echo $SNOWFLAKE_SNAPSHOT_DATABASE
    - export CI_PROFILE_TARGET="--profiles-dir profile --target ci"
    - echo $CI_PROFILE_TARGET
    - export DBT_RUNNER="${GITLAB_USER_ID}-${CI_MERGE_REQUEST_ID}"
    - echo $DBT_RUNNER
    - mkdir -p ~/.ssh
    - touch ~/.ssh/id_rsa
    - chmod 700 ~/.ssh
    - echo "$GIT_DATA_TESTS_SSH_PRIVATE_KEY" | base64 --decode > ~/.ssh/id_rsa # decodes key from base64
    - chmod 0400 ~/.ssh/id_rsa # Makes key read only
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config # Adds gitlab.com as known host
  tags:
    - analytics
  when: manual
  only:
    changes:
      - "transform/snowflake-dbt/models/**/*.sql"
      - "transform/snowflake-dbt/analysis/**/*.sql"
    refs:
      - merge_request
  allow_failure: true

🛃dbt_sqlfluff:
  <<: *dbt_sql_lint
  script:
    - dbt deps $CI_PROFILE_TARGET
    - export LINT_LIST=$(git diff origin/master...HEAD --name-only -- '*.sql'  | awk -F 'transform/snowflake-dbt/' '{print tolower($NF)}' | sort | uniq )
    - echo $LINT_LIST
    - sqlfluff lint $LINT_LIST --config .sqlfluff-ci
