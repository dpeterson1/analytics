connection_info:
  postgres_source_connection:
    database: GITLAB_COM_CI_DB_NAME
    host: GITLAB_COM_CI_DB_HOST
    pass: GITLAB_COM_CI_DB_PASS
    port: GITLAB_COM_CI_DB_PORT
    user: GITLAB_COM_CI_DB_USER
  postgres_metadata_connection:
    database: GITLAB_METADATA_DB_NAME
    host: GITLAB_METADATA_DB_HOST
    pass: GITLAB_METADATA_DB_PASS
    port: GITLAB_METADATA_PG_PORT
    user: GITLAB_METADATA_DB_USER
tables:
  ci_builds:
    dbt_source_model: true
    deletes_exempt: True
    export_schema: 'gitlab_com'
    export_table: ci_builds
    export_table_primary_key: id
    import_db: GITLAB_DB
    import_query: |-
      SELECT id
      , status
      , finished_at
      , created_at
      , updated_at
      , started_at
      , runner_id
      , coverage
      , commit_id
      , CASE
            WHEN name ilike '%%container_scanning%%' THEN 'container_scanning'
            WHEN name ilike '%%dast%%' THEN 'dast'
            WHEN name ilike '%%dependency_scanning%%' THEN 'dependency_scanning'
            WHEN name ilike '%%license_management%%' THEN 'license_management'
            WHEN name ilike '%%license_scanning%%' THEN 'license_scanning'
            WHEN name ilike '%%sast%%' THEN 'sast'
            WHEN name ilike '%%secret_detection%%' THEN 'secret_detection'
            WHEN name ilike '%%coverage_fuzzing%%' THEN 'coverage_fuzzing'
            WHEN name ilike '%%apifuzzer_fuzz%%' THEN 'apifuzzer_fuzz'
            WHEN name ilike '%%apifuzzer_fuzz_dnd%%' THEN 'apifuzzer_fuzz_dnd'
            ELSE NULL
        END as name -- https://gitlab.com/gitlab-data/analytics/-/issues/18231#note_1529996846
      , NULL AS options
      , allow_failure
      , NULL AS stage
      , trigger_request_id
      , stage_idx
      , tag
      , NULL AS ref
      , user_id
      , type
      , NULL AS description
      , project_id
      , erased_by_id
      , erased_at
      , CASE WHEN artifacts_expire_at > '2262-01-01' THEN '2262-01-01' ELSE artifacts_expire_at END AS artifacts_expire_at
      , NULL AS environment
      , queued_at
      , lock_version
      , coverage_regex
      , auto_canceled_by_id
      , retried
      , stage_id
      , protected
      , failure_reason
      , scheduled_at
      , upstream_pipeline_id
      FROM p_ci_builds
      WHERE updated_at BETWEEN '{BEGIN_TIMESTAMP}'::timestamp
        AND '{END_TIMESTAMP}'::timestamp
  ci_builds_internal_only:
    additional_filtering: AND project_id in {INTERNAL_PROJECT_IDS}
    dbt_source_model: true
    deletes_exempt: True
    export_schema: 'gitlab_com'
    export_table: ci_builds_internal_only
    export_table_primary_key: id
    import_db: GITLAB_DB
    import_query: |-
      SELECT id
      , updated_at
      , name
      , stage
      , ref
      , description
      , project_id
      FROM p_ci_builds
      WHERE updated_at BETWEEN '{BEGIN_TIMESTAMP}'::timestamp
        AND '{END_TIMESTAMP}'::timestamp
  ci_secure_files:
    dbt_source_model: true
    deletes_exempt: True
    export_schema: 'gitlab_com'
    export_table: ci_secure_files
    export_table_primary_key: id
    import_db: GITLAB_DB
    import_query: |-
      SELECT id
      , project_id
      , created_at
      , updated_at
      , file_store
      FROM ci_secure_files
      WHERE updated_at BETWEEN '{BEGIN_TIMESTAMP}'::timestamp
        AND '{END_TIMESTAMP}'::timestamp
  ci_group_variables:
    dbt_source_model: true
    deletes_exempt: True
    export_schema: 'gitlab_com'
    export_table: ci_group_variables
    export_table_primary_key: id
    import_db: GITLAB_DB
    import_query: |-
      SELECT id
      , group_id
      , created_at
      , updated_at
      , masked
      , variable_type
      FROM ci_group_variables
      WHERE updated_at BETWEEN '{BEGIN_TIMESTAMP}'::timestamp
        AND '{END_TIMESTAMP}'::timestamp
  ci_job_artifacts:
    additional_filtering: AND created_at NOT IN ( '0001-01-01 00:00:00+00', '1000-01-01 00:00:00+00', '10000-01-01 00:00:00+00')
    dbt_source_model: true
    export_schema: 'gitlab_com'
    export_table: ci_job_artifacts
    export_table_primary_key: id
    import_db: GITLAB_DB
    import_query: |-
      SELECT id
      , project_id
      , job_id
      , file_type
      , size
      , created_at
      , updated_at
      , CASE WHEN expire_at > '2262-01-01' THEN '2262-01-01' ELSE expire_at END AS expire_at
      , NULL AS file
      , file_store
      , file_format
      , file_location
      , locked
      FROM p_ci_job_artifacts
      WHERE updated_at BETWEEN '{BEGIN_TIMESTAMP}'::timestamp
        AND '{END_TIMESTAMP}'::timestamp
  ci_pipeline_artifacts:
    dbt_source_model: true
    deletes_exempt: True
    export_schema: 'gitlab_com'
    export_table: ci_pipeline_artifacts
    export_table_primary_key: id
    import_db: GITLAB_DB
    import_query: |-
      SELECT id
      , created_at
      , updated_at
      , pipeline_id
      , project_id
      , size
      , file_store
      , file_type
      , file_format
      , file
      , expire_at
      , locked
      FROM ci_pipeline_artifacts
      WHERE updated_at BETWEEN '{BEGIN_TIMESTAMP}'::timestamp
        AND '{END_TIMESTAMP}'::timestamp
  ci_pipeline_schedule_variables:
    dbt_source_model: true
    deletes_exempt: True
    export_schema: 'gitlab_com'
    export_table: ci_pipeline_schedule_variables
    export_table_primary_key: id
    import_db: GITLAB_DB
    import_query: |-
      SELECT id
      , pipeline_schedule_id
      , created_at
      , updated_at
      , variable_type
      FROM ci_pipeline_schedule_variables
      WHERE updated_at BETWEEN '{BEGIN_TIMESTAMP}'::timestamp
        AND '{END_TIMESTAMP}'::timestamp
  ci_pipeline_schedules:
    dbt_source_model: true
    deletes_exempt: True
    export_schema: 'gitlab_com'
    export_table: ci_pipeline_schedules
    export_table_primary_key: id
    import_db: GITLAB_DB
    import_query: |-
      SELECT id
      , NULL AS ref
      , next_run_at
      , project_id
      , owner_id
      , active
      , created_at
      , updated_at
      FROM ci_pipeline_schedules
      WHERE updated_at BETWEEN '{BEGIN_TIMESTAMP}'::timestamp
        AND '{END_TIMESTAMP}'::timestamp
  ci_pipelines:
    dbt_source_model: true
    deletes_exempt: True
    export_schema: 'gitlab_com'
    export_table: 'ci_pipelines'
    export_table_primary_key: id
    import_db: GITLAB_DB
    import_query: |-
      SELECT id
      , created_at
      , updated_at
      , NULL AS ref
      , tag
      , NULL AS yaml_errors
      , committed_at
      , project_id
      , status
      , started_at
      , finished_at
      , duration
      , user_id
      , lock_version
      , auto_canceled_by_id
      , pipeline_schedule_id
      , source
      , config_source
      , protected
      , failure_reason
      , iid
      , merge_request_id
      FROM ci_pipelines
      WHERE updated_at BETWEEN '{BEGIN_TIMESTAMP}'::timestamp
        AND '{END_TIMESTAMP}'::timestamp
  ci_pipelines_internal_only:
    additional_filtering: AND project_id in {INTERNAL_PROJECT_IDS}
    dbt_source_model: true
    deletes_exempt: True
    export_schema: 'gitlab_com'
    export_table: 'ci_pipelines_internal_only'
    export_table_primary_key: id
    import_db: GITLAB_DB
    import_query: |-
      SELECT id
      , updated_at
      , ref
      , project_id
      FROM ci_pipelines
      WHERE updated_at BETWEEN '{BEGIN_TIMESTAMP}'::timestamp
        AND '{END_TIMESTAMP}'::timestamp
  ci_platform_metrics:
    dbt_source_model: true
    deletes_exempt: True
    export_schema: 'gitlab_com'
    export_table: ci_platform_metrics
    export_table_primary_key: id
    import_db: GITLAB_DB
    import_query: |-
      SELECT id
      , recorded_at
      , platform_target
      , count
      FROM ci_platform_metrics
      WHERE recorded_at BETWEEN '{BEGIN_TIMESTAMP}'::timestamp
        AND '{END_TIMESTAMP}'::timestamp
  ci_runner_projects:
    dbt_source_model: true
    deletes_exempt: True
    export_schema: 'gitlab_com'
    export_table: ci_runner_projects
    export_table_primary_key: id
    import_db: GITLAB_DB
    import_query: |-
      SELECT id
      , runner_id
      , created_at
      , updated_at
      , project_id
      FROM ci_runner_projects
      WHERE updated_at BETWEEN '{BEGIN_TIMESTAMP}'::timestamp
        AND '{END_TIMESTAMP}'::timestamp
  ci_runners:
    # We only extract ci_runners descriptions for runner_type=1 because they're hosted and managed by GitLab, and the other runner_types desciptions are classified as RED data, hence description is are not extracted. More here https://gitlab.com/gitlab-data/analytics/-/issues/18358#note_1563889597
    dbt_source_model: true
    deletes_exempt: True
    export_schema: 'gitlab_com'
    export_table: ci_runners
    export_table_primary_key: id
    import_db: GITLAB_DB
    import_query: |-
      SELECT id
      , created_at
      , updated_at
      , CASE
            WHEN runner_type = 1 THEN description
            ELSE NULL
        END AS description
      , contacted_at
      , active
      , version
      , revision
      , platform
      , run_untagged
      , locked
      , access_level
      , maximum_timeout
      , runner_type
      , public_projects_minutes_cost_factor
      , private_projects_minutes_cost_factor
      FROM ci_runners
      WHERE updated_at BETWEEN '{BEGIN_TIMESTAMP}'::timestamp
        AND '{END_TIMESTAMP}'::timestamp
  ci_stages:
    dbt_source_model: true
    export_schema: 'gitlab_com'
    export_table: ci_stages
    export_table_primary_key: id
    import_db: GITLAB_DB
    import_query: |-
      SELECT id
      , project_id
      , pipeline_id
      , created_at
      , updated_at
      , NULL AS name
      , status
      , lock_version
      , position
      FROM p_ci_stages
      WHERE updated_at BETWEEN '{BEGIN_TIMESTAMP}'::timestamp
        AND '{END_TIMESTAMP}'::timestamp
  ci_triggers:
    dbt_source_model: true
    deletes_exempt: True
    export_schema: 'gitlab_com'
    export_table: ci_triggers
    export_table_primary_key: id
    import_db: GITLAB_DB
    import_query: |-
      SELECT id
      , created_at
      , updated_at
      , project_id
      , owner_id
      FROM ci_triggers
      WHERE updated_at BETWEEN '{BEGIN_TIMESTAMP}'::timestamp
        AND '{END_TIMESTAMP}'::timestamp
